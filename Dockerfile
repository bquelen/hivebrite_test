from ruby:2.6-slim-buster

RUN mkdir /app

ADD . /app/

WORKDIR /app/

RUN gem install bundler -v 2.0.2

RUN apt update && apt install -y build-essential patch ruby-dev zlib1g-dev liblzma-dev libsqlite3-dev curl

RUN  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn

RUN bundle install

RUN yarn install

CMD ["bundle", "exec", "rspec"]
