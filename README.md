# Hivebrite Test

## Usage

### Init the service class for User profil
service = CustomFieldService.new("User", CustomField::PROFILE_USAGE, CustomField::GLOBAL)

### Adding à field
service.add_field("school", CustomField::TEXT_FIELD, false)

### Removing a field
service.remove_field("school")

### Getting fields info
service.get_fields()

### Saving data using params hash add passing user id
service.save(params, user_id)

### Getting data back using user id
service.get_data(user_id)

## Docker

### For creating image 
docker build -t hivebrite_test .

### For running  rspec
docker run -it hivebrite_test

### For console
docker run -it hivebrite_test rails db:migrate

docker run -it hivebrite_test rails db:seed

docker run -it hivebrite_test rails c

