class CustomField < ApplicationRecord

  validates :customizable_model, presence: true
  validates :customizable_usage, presence: true

  serialize :form_data, JSON

  before_validation :init_form_data

  # id for apply on all class
  GLOBAL = -1

  # Field type
  BOOLEAN_FIELD = "boolean"
  TEXT_FIELD = "text"

  # usage
  REGISTER_USAGE = "register"
  PROFILE_USAGE = "profil"

  #field name
  NAME = "name"
  TYPE = "type"
  REQUIRED = "required"

  def init_form_data
    if form_data.nil?
      form_data = []
    end
  end

  def add_field(name, field_type, required = false)
    existing_field = form_data.select{|f| f[NAME] == name}
    if existing_field.size > 0 && existing_field.first[TYPE] != field_type
      raise CustomFieldException.new("field type is different")
    end
    if existing_field.size.zero?
      form_data.push({ name: name,
                                     type: field_type,
                                     required: required })
    else
      idx = form_data.find_index{|f| f[NAME] == name}
      form_data[idx] = { name: name,
                                       type: field_type,
                                       required: required }
    end
    self.save
  end

end
