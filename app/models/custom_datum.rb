class CustomDatum < ApplicationRecord

  belongs_to :customizable, polymorphic: true
  belongs_to :custom_field

  serialize :data, JSON

  before_validation :init_data

  # field name
  NAME = "name"
  VALUE = "value"

  def init_data
    if data.nil?
      data = []
    end
  end

  def self.from_custom_field(custom_field, id)
    datum = CustomDatum.where(custom_field_id: custom_field.id,
                              customizable_type: custom_field.customizable_model,
                              customizable_id: id).first
    if datum.nil?
      datum = CustomDatum.create(custom_field_id: custom_field.id,
                                customizable_type: custom_field.customizable_model,
                                customizable_id: id,
                                data: [])
    end
    datum
  end

  def save_data(params)
    custom_field.form_data.each do |field|
      if params.has_key? field[CustomField::NAME].to_sym
        data_idx = data.find_index{|d| d[NAME] == field[CustomField::NAME]}
        val = params[field[CustomField::NAME].to_sym]


        if field[CustomField::TYPE] == CustomField::BOOLEAN_FIELD && ![true, false].include?(val)
            raise CustomFieldException.new("#{CustomField::NAME} has a bad type")
        end

        if field[CustomField::TYPE] == CustomField::TEXT_FIELD && !val.kind_of?(String)
            raise CustomFieldException.new("#{CustomField::NAME} has a bad type")
        end

        if data_idx.present?
          data[data_idx] = { NAME => field[CustomField::NAME],
                             VALUE => val }
        else
          data.push({ NAME => field[CustomField::NAME],
                      VALUE => val})
        end
      else
        if field[CustomField::REQUIRED] == true
          raise CustomFieldException.new("#{CustomField::NAME} is required")
        end
      end
    end
    return self.save

  end

end
