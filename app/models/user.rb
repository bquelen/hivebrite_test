class User < ApplicationRecord

  validates :name, presence: true

  def custom_profile_fields
    CustomField.find_by(customizable_model: "User", customizable_usage: "profil")
  end

  def custom_register_fields
    CustomField.find_by(customizable_model: "User", customizable_usage: "register")
  end


end
