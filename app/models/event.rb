class Event < ApplicationRecord

  validates :name, presence: true

  def custom_register_fields
    CustomField.find_by(customizable_model: "User", customizable_usage: "register")
  end

end
