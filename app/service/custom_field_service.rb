class CustomFieldException < StandardError
end

class CustomFieldService

  def initialize(class_name, usage = CustomField::REGISTER_USAGE, id = nil)
    @custom_field = CustomField.where(customizable_model: class_name,
                                      customizable_usage: usage)
    @custom_field = @custom_field.where(customizable_id: id) if id.present?
    @custom_field = @custom_field.first
  end

  def add_field(name, field_type, required = false)
    raise CustomFieldException.new("missing name") if name.blank?

    @custom_field.add_field(name, field_type, required)
  end

  def remove_field(name)
    @custom_field.form_data.delete_if{|f| f[CustomField::NAME] == name}
    @custom_field.save
  end

  def get_fields
    @custom_field.form_data
  end

  def save(params = {}, id)
    datum = CustomDatum.from_custom_field(@custom_field, id)
    datum.save_data(params)
  end

  def get_data(id = nil)
    datum = CustomDatum.from_custom_field(@custom_field, id)
    datum.data
  end

end
