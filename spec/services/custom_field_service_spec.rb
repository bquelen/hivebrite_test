require 'spec_helper'
require 'rails_helper'

describe CustomFieldService do
  let!(:custom_field) { CustomField.create(customizable_model: "User",
                                          customizable_usage: CustomField::PROFILE_USAGE,
                                          customizable_id: CustomField::GLOBAL,
                                          form_data: []) }
  let(:service) { CustomFieldService.new("User",
                                          CustomField::PROFILE_USAGE,
                                          CustomField::GLOBAL) }

  describe '.add_field' do
    context 'creating fields' do
      it ' with missing name' do
        expect{ service.add_field("", "", false) }.to raise_error(CustomFieldException)
      end

      it ' with good data' do
        expect(service.add_field("member", CustomField::BOOLEAN_FIELD, true)).to eq(true)
        expect(service.get_fields().any?{|f| f[CustomField::NAME] == "member"}).to be true
      end

      it ' with same name different type' do
        service.add_field("member", CustomField::BOOLEAN_FIELD, true)
        expect{ service.add_field("member", CustomField::TEXT_FIELD, true) }.to raise_error(CustomFieldException)
      end

      it ' with same name different require' do
        service.add_field("member", CustomField::BOOLEAN_FIELD, false)
        expect(service.add_field("member", CustomField::BOOLEAN_FIELD, true)).to eq(true)
        expect(service.get_fields.select{|f| f[CustomField::NAME] == "member"}.first[CustomField::REQUIRED]).to eq(true)
      end

      it ' and removing field' do
        service.add_field("member", CustomField::BOOLEAN_FIELD, false)
        service.add_field("school", CustomField::TEXT_FIELD, false)
        service.remove_field("member")
        expect(service.get_fields.any?{|f| f[CustomField::NAME] == "school"}).to eq(true)
        expect(service.get_fields.any?{|f| f[CustomField::NAME] == "member"}).to eq(false)
      end
    end
  end

  describe '.save' do
    let(:user) { User.create(name: "test user") }
    let(:good_params) { { school: "test school" } }
    let(:bad_type_params) { { member: "test", "school": true } }

    context 'manipulating data' do
      it ' inserting good data' do
        service.add_field("school", CustomField::TEXT_FIELD, false)
        service.save(good_params, user.id)
        expect(service.get_data(user.id)[0]["value"]).to eq("test school")
      end

      it ' insert data with a missing field' do
        service.add_field("school", CustomField::TEXT_FIELD, false)
        service.add_field("company", CustomField::TEXT_FIELD, true)
        expect{service.save(good_params, user.id)}.to raise_error(CustomFieldException)
      end

      it ' insert data with bad type for bool' do
        service.add_field("member", CustomField::BOOLEAN_FIELD, false)
        expect{service.save(bad_type_params, user.id)}.to raise_error(CustomFieldException)
      end

      it ' insert data with bad type for string' do
        service.add_field("school", CustomField::TEXT_FIELD, false)
        expect{service.save(bad_type_params, user.id)}.to raise_error(CustomFieldException)
      end

    end
  end

  describe '.save for Event' do
    let!(:event) { Event.create(name: "test event") }
    let!(:custom_field_event) { CustomField.create(customizable_model: "Event",
                                            customizable_usage: CustomField::REGISTER_USAGE,
                                            customizable_id: event.id,
                                            form_data: []) }
    let(:service) { CustomFieldService.new("Event",
                                          CustomField::REGISTER_USAGE,
                                          event.id) }
    let(:params) { { number_guest: "3" } }

    it 'add field and save data' do
      service.add_field("number_guest", CustomField::TEXT_FIELD, false)
      service.save(params, event.id)
      expect(service.get_data(event.id)[0]["value"]).to eq("3")
    end
  end
end
