class CreateCustomData < ActiveRecord::Migration[6.0]
  def change
    create_table :custom_data do |t|
      t.integer :custom_field_id
      t.string :customizable_type
      t.integer :customizable_id
      t.text :data

      t.timestamps
    end
    add_index :custom_data, :custom_field_id, unique: true
    add_index :custom_data, [:customizable_type, :customizable_id], unique: true, name: "custom_relation"
  end
end
