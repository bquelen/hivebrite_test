class CreateCustomFields < ActiveRecord::Migration[6.0]
  def change
    create_table :custom_fields do |t|
      t.string :customizable_model
      t.text :form_data
      t.string :customizable_usage
      t.integer :customizable_id

      t.timestamps
    end
    add_index :custom_fields, [:customizable_model, :customizable_usage, :customizable_id], unique: true, name: "custom_model"
  end
end
